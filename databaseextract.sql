-- MySQL dump 10.13  Distrib 5.6.22, for osx10.8 (x86_64)
--
-- Host: localhost    Database: jamar_db
-- ------------------------------------------------------
-- Server version	5.6.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apartments`
--

DROP TABLE IF EXISTS `apartments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apartments` (
  `apartment_id` int(11) NOT NULL,
  `monthly_rent` int(11) NOT NULL,
  `notes` longtext NOT NULL,
  `apartment_label` varchar(12) NOT NULL,
  `building_id` int(11) NOT NULL,
  PRIMARY KEY (`apartment_id`),
  KEY `apartments_f0aa7168` (`building_id`),
  CONSTRAINT `building_id_refs_building_id_fd3419e9` FOREIGN KEY (`building_id`) REFERENCES `buildings` (`building_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `apartments`
--

LOCK TABLES `apartments` WRITE;
/*!40000 ALTER TABLE `apartments` DISABLE KEYS */;
/*!40000 ALTER TABLE `apartments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_0e939a4f` (`group_id`),
  KEY `auth_group_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_417f1b1c` (`content_type_id`),
  CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add apartments',6,'add_apartments'),(17,'Can change apartments',6,'change_apartments'),(18,'Can delete apartments',6,'delete_apartments'),(19,'Can add buildings',7,'add_buildings'),(20,'Can change buildings',7,'change_buildings'),(21,'Can delete buildings',7,'delete_buildings'),(22,'Can add company',8,'add_company'),(23,'Can change company',8,'change_company'),(24,'Can delete company',8,'delete_company'),(25,'Can add computer person',9,'add_computerperson'),(26,'Can change computer person',9,'change_computerperson'),(27,'Can delete computer person',9,'delete_computerperson'),(28,'Can add deposit refunds',10,'add_depositrefunds'),(29,'Can change deposit refunds',10,'change_depositrefunds'),(30,'Can delete deposit refunds',10,'delete_depositrefunds'),(31,'Can add landlords',11,'add_landlords'),(32,'Can change landlords',11,'change_landlords'),(33,'Can delete landlords',11,'delete_landlords'),(34,'Can add person',12,'add_person'),(35,'Can change person',12,'change_person'),(36,'Can delete person',12,'delete_person'),(37,'Can add refund deductions',13,'add_refunddeductions'),(38,'Can change refund deductions',13,'change_refunddeductions'),(39,'Can delete refund deductions',13,'delete_refunddeductions'),(40,'Can add rent refunds',14,'add_rentrefunds'),(41,'Can change rent refunds',14,'change_rentrefunds'),(42,'Can delete rent refunds',14,'delete_rentrefunds'),(43,'Can add rental payment',15,'add_rentalpayment'),(44,'Can change rental payment',15,'change_rentalpayment'),(45,'Can delete rental payment',15,'delete_rentalpayment'),(46,'Can add session',16,'add_session'),(47,'Can change session',16,'change_session'),(48,'Can delete session',16,'delete_session'),(49,'Can add tenants',17,'add_tenants'),(50,'Can change tenants',17,'change_tenants'),(51,'Can delete tenants',17,'delete_tenants');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `buildings`
--

DROP TABLE IF EXISTS `buildings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `buildings` (
  `building_id` int(11) NOT NULL,
  `address_id` int(11) NOT NULL,
  `landlord_id` int(11) NOT NULL,
  `commission` int(11) NOT NULL,
  `building_name` varchar(20) NOT NULL,
  `notes` longtext NOT NULL,
  `landlord_permission` varchar(3) NOT NULL,
  PRIMARY KEY (`building_id`),
  KEY `buildings_ee1dd250` (`landlord_id`),
  CONSTRAINT `landlord_id_refs_landlord_id_de6f1478` FOREIGN KEY (`landlord_id`) REFERENCES `landlords` (`landlord_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `buildings`
--

LOCK TABLES `buildings` WRITE;
/*!40000 ALTER TABLE `buildings` DISABLE KEYS */;
/*!40000 ALTER TABLE `buildings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `address_id` int(11) NOT NULL,
  `phone_number` varchar(12) NOT NULL,
  `email_address` varchar(20) NOT NULL,
  `receipt_height` int(11) NOT NULL,
  `receipt_width` int(11) NOT NULL,
  `receipt_background` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `computer_person`
--

DROP TABLE IF EXISTS `computer_person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `computer_person` (
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `username_id` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `rest_password` varchar(30) NOT NULL,
  `person_id` int(11) NOT NULL,
  `access_type` varchar(5) NOT NULL,
  `is_admin` tinyint(1) NOT NULL,
  PRIMARY KEY (`username_id`),
  UNIQUE KEY `username` (`username`),
  KEY `computer_person_16f39487` (`person_id`),
  CONSTRAINT `person_id_refs_person_id_ac2df84e` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `computer_person`
--

LOCK TABLES `computer_person` WRITE;
/*!40000 ALTER TABLE `computer_person` DISABLE KEYS */;
/*!40000 ALTER TABLE `computer_person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deposit_refunds`
--

DROP TABLE IF EXISTS `deposit_refunds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deposit_refunds` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `amount` int(11) NOT NULL,
  `form` varchar(6) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `deposit_refunds_1384d482` (`tenant_id`),
  CONSTRAINT `tenant_id_refs_tenant_id_fa294954` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deposit_refunds`
--

LOCK TABLES `deposit_refunds` WRITE;
/*!40000 ALTER TABLE `deposit_refunds` DISABLE KEYS */;
/*!40000 ALTER TABLE `deposit_refunds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_417f1b1c` (`content_type_id`),
  KEY `django_admin_log_e8701ad4` (`user_id`),
  CONSTRAINT `djang_content_type_id_697914295151027a_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_a_user_id_52fdd58701c5f563_fk_computer_person_username_id` FOREIGN KEY (`user_id`) REFERENCES `computer_person` (`username_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'log entry','admin','logentry'),(2,'permission','auth','permission'),(3,'group','auth','group'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'apartments','propertymanagement','apartments'),(7,'buildings','propertymanagement','buildings'),(8,'company','propertymanagement','company'),(9,'computer person','propertymanagement','computerperson'),(10,'deposit refunds','propertymanagement','depositrefunds'),(11,'landlords','propertymanagement','landlords'),(12,'person','propertymanagement','person'),(13,'refund deductions','propertymanagement','refunddeductions'),(14,'rent refunds','propertymanagement','rentrefunds'),(15,'rental payment','propertymanagement','rentalpayment'),(16,'session','propertymanagement','session'),(17,'tenants','propertymanagement','tenants');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2015-01-28 15:56:30'),(2,'admin','0001_initial','2015-01-28 15:56:30'),(3,'auth','0001_initial','2015-01-28 15:56:31'),(4,'sessions','0001_initial','2015-01-28 15:56:31');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `landlords`
--

DROP TABLE IF EXISTS `landlords`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `landlords` (
  `landlord_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `time` datetime NOT NULL,
  PRIMARY KEY (`landlord_id`),
  KEY `landlords_16f39487` (`person_id`),
  CONSTRAINT `person_id_refs_person_id_07134679` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `landlords`
--

LOCK TABLES `landlords` WRITE;
/*!40000 ALTER TABLE `landlords` DISABLE KEYS */;
/*!40000 ALTER TABLE `landlords` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `person` (
  `person_id` int(11) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `middle_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email_address` varchar(12) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `identification` varchar(12) NOT NULL,
  `address_id` int(11) NOT NULL,
  `date_time` datetime NOT NULL,
  PRIMARY KEY (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `refund_deductions`
--

DROP TABLE IF EXISTS `refund_deductions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `refund_deductions` (
  `deduction_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `type` varchar(12) NOT NULL,
  `comment` longtext NOT NULL,
  `time` datetime NOT NULL,
  `landlord_id` int(11) NOT NULL,
  PRIMARY KEY (`deduction_id`),
  KEY `refund_deductions_ee1dd250` (`landlord_id`),
  CONSTRAINT `landlord_id_refs_landlord_id_d8c60540` FOREIGN KEY (`landlord_id`) REFERENCES `landlords` (`landlord_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `refund_deductions`
--

LOCK TABLES `refund_deductions` WRITE;
/*!40000 ALTER TABLE `refund_deductions` DISABLE KEYS */;
/*!40000 ALTER TABLE `refund_deductions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rent_refunds`
--

DROP TABLE IF EXISTS `rent_refunds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rent_refunds` (
  `refund_id` int(11) NOT NULL,
  `refund_time` datetime NOT NULL,
  `refund_amount` int(11) NOT NULL,
  `landlord_id` int(11) NOT NULL,
  PRIMARY KEY (`refund_id`),
  KEY `rent_refunds_ee1dd250` (`landlord_id`),
  CONSTRAINT `landlord_id_refs_landlord_id_a94c2d0c` FOREIGN KEY (`landlord_id`) REFERENCES `landlords` (`landlord_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rent_refunds`
--

LOCK TABLES `rent_refunds` WRITE;
/*!40000 ALTER TABLE `rent_refunds` DISABLE KEYS */;
/*!40000 ALTER TABLE `rent_refunds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rental_payment`
--

DROP TABLE IF EXISTS `rental_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `rental_payment` (
  `receipt_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `form` varchar(6) NOT NULL,
  `purpose` varchar(7) NOT NULL,
  `time` datetime NOT NULL,
  `receipt_type` varchar(5) NOT NULL,
  `apartment_id` int(11) NOT NULL,
  `tenant_id` int(11) NOT NULL,
  PRIMARY KEY (`receipt_id`),
  KEY `rental_payment_fd482c61` (`apartment_id`),
  KEY `rental_payment_1384d482` (`tenant_id`),
  CONSTRAINT `apartment_id_refs_apartment_id_0bc1b1be` FOREIGN KEY (`apartment_id`) REFERENCES `apartments` (`apartment_id`),
  CONSTRAINT `tenant_id_refs_tenant_id_d1bd1301` FOREIGN KEY (`tenant_id`) REFERENCES `tenants` (`tenant_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rental_payment`
--

LOCK TABLES `rental_payment` WRITE;
/*!40000 ALTER TABLE `rental_payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `rental_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `session` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `login_history` datetime NOT NULL,
  `access_device` varchar(7) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tenants`
--

DROP TABLE IF EXISTS `tenants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tenants` (
  `tenant_id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `deposit_id` int(11) NOT NULL,
  `notes` longtext NOT NULL,
  `status` varchar(7) NOT NULL,
  PRIMARY KEY (`tenant_id`),
  KEY `tenants_16f39487` (`person_id`),
  CONSTRAINT `person_id_refs_person_id_15890400` FOREIGN KEY (`person_id`) REFERENCES `person` (`person_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tenants`
--

LOCK TABLES `tenants` WRITE;
/*!40000 ALTER TABLE `tenants` DISABLE KEYS */;
/*!40000 ALTER TABLE `tenants` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-01-28 11:09:16
