# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager

from django.db import models


class Apartments(models.Model):
    apartment_id = models.IntegerField(primary_key=True)
    monthly_rent = models.IntegerField()
    notes = models.TextField(blank=True)
    apartment_label = models.CharField(max_length=12, blank=True)
    building = models.ForeignKey('Buildings')

    class Meta:
        db_table = 'apartments'


class Buildings(models.Model):
    building_id = models.IntegerField(primary_key=True)
    address_id = models.IntegerField()
    landlord = models.ForeignKey('Landlords')
    commission = models.IntegerField()
    building_name = models.CharField(max_length=20, blank=True)
    notes = models.TextField(blank=True)
    landlord_permission = models.CharField(max_length=3, blank=True)

    class Meta:
        db_table = 'buildings'


class Company(models.Model):
    name = models.CharField(max_length=20)
    address_id = models.IntegerField()
    phone_number = models.CharField(max_length=12)
    email_address = models.CharField(max_length=20)
    receipt_height = models.IntegerField()
    receipt_width = models.IntegerField()
    receipt_background = models.TextField(blank=True)

    class Meta:
        db_table = 'company'


class ComputerPerson(AbstractBaseUser):
    username_id = models.IntegerField (primary_key=True)
    username = models.CharField(max_length=10, unique=True,blank=False)
    rest_password = models.CharField(max_length=30, blank=False)
    person = models.ForeignKey('Person')
    access_type = models.CharField(max_length=5, blank=False)
    is_admin = models.BooleanField(default=False)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['person']
    def is_active(self):
        return True
    def get_full_name(self):
        return self.person.firstname + " " + self.person.last_name
    def get_short_name(self):
        return self.person.firstname
    class Meta:
        db_table = 'computer_person'


class DepositRefunds(models.Model):
    time = models.DateTimeField()
    amount = models.IntegerField()
    form = models.CharField(max_length=6, blank=True)
    tenant = models.ForeignKey('Tenants')

    class Meta:
        db_table = 'deposit_refunds'


class Landlords(models.Model):
    landlord_id = models.IntegerField(primary_key=True)
    person = models.ForeignKey('Person')
    time = models.DateTimeField()

    class Meta:
        db_table = 'landlords'


class Person(models.Model):
    person_id = models.IntegerField(primary_key=True)
    first_name = models.CharField(max_length=30)
    middle_name = models.CharField(max_length=30, blank=True)
    last_name = models.CharField(max_length=30)
    email_address = models.CharField(max_length=12, blank=True)
    phone = models.CharField(max_length=30, blank=True)
    identification = models.CharField(max_length=12, blank=True)
    address_id = models.IntegerField()
    date_time = models.DateTimeField()

    class Meta:
        db_table = 'person'


class RefundDeductions(models.Model):
    deduction_id = models.IntegerField(primary_key=True)
    amount = models.IntegerField()
    type = models.CharField(max_length=12, blank=True)
    comment = models.TextField(blank=True)
    time = models.DateTimeField()
    landlord = models.ForeignKey(Landlords)

    class Meta:
        db_table = 'refund_deductions'


class RentRefunds(models.Model):
    refund_id = models.IntegerField(primary_key=True)
    refund_time = models.DateTimeField()
    refund_amount = models.IntegerField()
    landlord = models.ForeignKey(Landlords)

    class Meta:
        db_table = 'rent_refunds'


class RentalPayment(models.Model):
    receipt_id = models.IntegerField(primary_key=True)
    amount = models.IntegerField()
    form = models.CharField(max_length=6, blank=True)
    purpose = models.CharField(max_length=7, blank=True)
    time = models.DateTimeField()
    receipt_type = models.CharField(max_length=5, blank=True)
    apartment = models.ForeignKey(Apartments)
    tenant = models.ForeignKey('Tenants')

    class Meta:
        db_table = 'rental_payment'


class Session(models.Model):
    username = models.CharField(max_length=10, blank=True)
    login_history = models.DateTimeField()
    access_device = models.CharField(max_length=7, blank=True)

    class Meta:
        db_table = 'session'


class Tenants(models.Model):
    tenant_id = models.IntegerField(primary_key=True)
    person = models.ForeignKey(Person)
    deposit_id = models.IntegerField()
    notes = models.TextField(blank=True)
    status = models.CharField(max_length=7, blank=True)

    class Meta:
        db_table = 'tenants'

class CustomUserManager(BaseUserManager):
    def create_user(self,email,person,access,password=None):
            if not email:
                raise ValueError('Users must have an email address')
            if not person:
                raise ValueError('Users must contain a person object')
            user = ComputerPerson()
            user.username = email
            user.person = person
            user.access_type = access
            user.save(using=self._db)
            return user

    def create_superuser(self,email,person,access,password=None):
            if not email:
                raise ValueError('Users must have an email address')
            if not person:
                raise ValueError('Users must contain a person object')
            user = ComputerPerson()
            user.username = email
            user.person = person
            user.access_type = access
            user.is_admin=True
            user.save(using=self._db)
            return user


